import type { JSONSchema7 } from 'json-schema';
import { validateBySchema } from '@avstantso/node-or-browser-js--model-core';
import { AxiosErrorCallback, ApiError, ParamsError } from './classes';
import MESSAGES from './messages';

export interface IContextError {
  params(s: string): ParamsError;
  validate<TData = any>(data: TData, schema: JSONSchema7): void;
  fromAxios: AxiosErrorCallback;
}

const ContextError = (model: string, method: string): IContextError => {
  const params = (s: string): ParamsError => new ParamsError(model, method, s);

  const validate = <TData = any>(data: TData, schema: JSONSchema7): void => {
    const { isValid, errors } = validateBySchema(data, schema);
    if (isValid) return;

    const e = new ParamsError(model, method, MESSAGES.dataFieldsMissing);

    e.details = errors;

    throw e;
  };

  return {
    params,
    validate,
    get fromAxios() {
      return ApiError.fromAxios(model, method);
    },
  };
};

export default ContextError;

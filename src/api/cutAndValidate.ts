import type { JSONSchema7 } from 'json-schema';
import { cutBySchema } from '@avstantso/node-or-browser-js--model-core';
import { IContextError } from '../contextError';

export default <TFrom, TTo>(
  entity: TFrom,
  schema: JSONSchema7,
  erc: IContextError
): TTo => {
  let r: TTo;
  try {
    r = cutBySchema<TFrom, TTo>(entity, schema);
  } catch {
    r = entity as any;
  }

  erc.validate(r, schema);

  return r;
};

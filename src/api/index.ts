import type { JSONSchema7 } from 'json-schema';
import { validate as isValidUUID } from 'uuid';
import Bluebird from 'bluebird';
import type { AxiosRequestConfig } from 'axios';
import {
  ID,
  IInsert,
  IUpdate,
  ISelect,
  ICondition,
  ISelectOptions,
  DataDecorationSetup,
  IData,
} from '@avstantso/node-or-browser-js--model-core';
import { dataDecorator } from '@avstantso/node-or-browser-js--model-core';
import { APIRListOverload, IAPIRW, ICreateAPIOptions } from '../types';
import ContextError from '../contextError';
import type { IAPIR, IAPIW } from '../types';
import MESSAGES from '../messages';
import makeTransformData from './transformData';
import cutAndValidate from './cutAndValidate';

export { default as cutAndValidate } from './cutAndValidate';

export const defaultDataDecoration: DataDecorationSetup = (dd) => dd.Dates;

const CreateAPI = (apiOptions: ICreateAPIOptions) => {
  const { ai, decorator } = apiOptions;

  const transformData = makeTransformData(
    decorator || dataDecorator(defaultDataDecoration)
  );

  const makeGetUrl = (getMasterUrl?: () => string) => (relUrl: string) =>
    `${getMasterUrl ? `${getMasterUrl()}/` : ''}${relUrl}`;

  const APIR = <TSelect extends ISelect, TCondition extends ICondition>(
    model: string,
    getMasterUrl?: () => string
  ): IAPIR<TSelect, TCondition> => {
    const getUrl = makeGetUrl(getMasterUrl);

    const list: APIRListOverload<TSelect, TCondition> = (
      ...params: any[]
    ): Promise<TSelect[]> => {
      const method = 'list';
      const erc = ContextError(getUrl(model), method);

      const isOptions = (param: any): boolean => {
        const p = param as ISelectOptions<TCondition>;
        return !!(p.pagination || p.search);
      };

      const options: ISelectOptions<TCondition> =
        (params.length > 0 && isOptions(params[0]) && params[0]) ||
        (params.length > 1 && isOptions(params[1]) && params[1]) ||
        undefined;
      const config: AxiosRequestConfig =
        (params.length > 0 && !isOptions(params[0]) && params[0]) ||
        (params.length > 1 && !isOptions(params[1]) && params[1]) ||
        undefined;

      // TODO
      if (options)
        return Bluebird.reject(Error(`TODO transfer options for "list"`));

      return Bluebird.resolve(ai.get<IData<TSelect[]>>(getUrl(model), config))
        .catch(erc.fromAxios)
        .then((res) => res.data?.data || [])
        .then((data) => data.map(transformData));
    };

    const one = (id: ID, config?: AxiosRequestConfig): Promise<TSelect> => {
      const method = 'one';
      const erc = ContextError(getUrl(model), method);

      if (!id) return Bluebird.reject(erc.params(MESSAGES.idNotSet));

      if (!isValidUUID(id))
        return Bluebird.reject(
          erc.params(`${MESSAGES.invalidUUIDFormatForId} "${id}"`)
        );

      return Bluebird.resolve(
        ai.get<IData<TSelect>>(getUrl(`${model}/${id}`), config)
      )
        .catch(erc.fromAxios)
        .then((res) => res.data?.data)
        .then(transformData);
    };

    return {
      list,
      one,
    };
  };

  const APIW = <
    TInsert extends IInsert,
    TUpdate extends IUpdate,
    TSelect extends ISelect
  >(
    model: string,
    schemaI: JSONSchema7,
    schemaU: JSONSchema7,
    getMasterUrl?: () => string
  ): IAPIW<TInsert, TUpdate, TSelect> => {
    const getUrl = makeGetUrl(getMasterUrl);

    const insertEntity = (
      entity: TInsert | TSelect,
      config?: AxiosRequestConfig
    ): Promise<TSelect> => {
      const method = 'insert';
      const erc = ContextError(getUrl(model), method);

      if (!entity) return Bluebird.reject(erc.params(MESSAGES.dataNotSet));

      return Bluebird.resolve()
        .then(() =>
          cutAndValidate<TInsert | TSelect, TInsert>(entity, schemaI, erc)
        )
        .then((data) =>
          ai
            .put<IData<TSelect>>(getUrl(model), { data }, config)
            .catch(erc.fromAxios)
        )
        .then((res) => res.data?.data)
        .then(transformData);
    };

    const updateEntity = (
      entity: TUpdate | TSelect,
      config?: AxiosRequestConfig
    ): Promise<TSelect> => {
      const method = 'update';
      const erc = ContextError(getUrl(model), method);

      if (!entity) return Bluebird.reject(erc.params(MESSAGES.dataNotSet));

      return Bluebird.resolve()
        .then(() =>
          cutAndValidate<TUpdate | TSelect, TUpdate>(entity, schemaU, erc)
        )
        .then((data) =>
          ai
            .post<IData<TSelect>>(getUrl(model), { data }, config)
            .catch(erc.fromAxios)
        )
        .then((res) => res.data?.data)
        .then(transformData);
    };

    const deleteEntity = (
      id: ID,
      config?: AxiosRequestConfig
    ): Promise<boolean> => {
      const method = 'delete';
      const erc = ContextError(getUrl(model), method);

      if (!id) return Bluebird.reject(erc.params(MESSAGES.idNotSet));

      if (!isValidUUID(id))
        return Bluebird.reject(
          erc.params(`${MESSAGES.invalidUUIDFormatForId} "${id}"`)
        );

      return Bluebird.resolve(
        ai.delete<IData<ID>>(getUrl(model), { ...(config || {}), data: { id } })
      )
        .catch(erc.fromAxios)
        .then((res) => !!res.data?.data);
    };

    return {
      insert: insertEntity,
      update: updateEntity,
      delete: deleteEntity,
    };
  };

  const APIRW = <
    TInsert extends IInsert,
    TUpdate extends IUpdate,
    TSelect extends ISelect,
    TCondition extends ICondition
  >(
    model: string,
    schemaI: JSONSchema7,
    schemaU: JSONSchema7,
    getMasterUrl?: () => string
  ): IAPIRW<TInsert, TUpdate, TSelect, TCondition> => ({
    ...APIR(model, getMasterUrl),
    ...APIW(model, schemaI, schemaU, getMasterUrl),
  });

  return { APIR, APIW, APIRW };
};

export default CreateAPI;

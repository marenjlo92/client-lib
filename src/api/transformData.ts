import { DataDecorator } from '@avstantso/node-or-browser-js--model-core';

export default <T = any>(decorator: DataDecorator<any>) => {
  const transformData = (data: any): T => {
    if (!data || 'object' !== typeof data) return data;

    const r = decorator(data);
    Object.keys(r).forEach((key) => {
      const value = (r as any)[key as keyof object];

      if (Array.isArray(value))
        return ((r as any)[key as keyof object] = value.map(transformData));

      if (!(value instanceof Date) && 'object' === typeof value)
        return ((r as any)[key as keyof object] = transformData(value));
    });

    return r;
  };

  return transformData;
};

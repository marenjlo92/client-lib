export interface IAuthorized {
  jwt: Readonly<string>;
}

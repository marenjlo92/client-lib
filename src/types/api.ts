import { AxiosInstance, AxiosRequestConfig } from 'axios';
import type {
  ID,
  IInsert,
  IUpdate,
  ISelect,
  ICondition,
  ISelectOptions,
  DataDecorator,
} from '@avstantso/node-or-browser-js--model-core';

export interface ICreateAPIOptions {
  ai: Readonly<AxiosInstance>;
  decorator?: DataDecorator;
}

export interface APIRListOverload<
  TSelect extends ISelect,
  TCondition extends ICondition
> {
  (options: ISelectOptions<TCondition>, config?: AxiosRequestConfig): Promise<
    TSelect[]
  >;
  (config: AxiosRequestConfig): Promise<TSelect[]>;
  (options?: ISelectOptions<TCondition>): Promise<TSelect[]>;
}

export interface IAPIR<TSelect extends ISelect, TCondition extends ICondition> {
  list: APIRListOverload<TSelect, TCondition>;
  one(id: ID, config?: AxiosRequestConfig): Promise<TSelect>;
}

export interface IAPIW<
  TInsert extends IInsert,
  TUpdate extends IUpdate,
  TSelect extends ISelect
> {
  insert(
    entity: TInsert | TSelect,
    config?: AxiosRequestConfig
  ): Promise<TSelect>;
  update(
    entity: TUpdate | TSelect,
    config?: AxiosRequestConfig
  ): Promise<TSelect>;
  delete(id: ID, config?: AxiosRequestConfig): Promise<boolean>;
}

export interface IAPIRW<
  TInsert extends IInsert,
  TUpdate extends IUpdate,
  TSelect extends ISelect,
  TCondition extends ICondition
> extends IAPIR<TSelect, TCondition>,
    IAPIW<TInsert, TUpdate, TSelect> {}

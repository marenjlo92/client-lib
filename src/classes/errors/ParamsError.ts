import { IMethodErrorReporter, MethodError } from './MethodError';

// local errors
export class ParamsError extends MethodError {
  constructor(
    model: string,
    method: string,
    message: string,
    internal?: Error
  ) {
    super(model, method, message, internal);

    Object.setPrototypeOf(this, ParamsError.prototype);
  }

  static is(error: Error): boolean {
    return error instanceof ParamsError;
  }

  static reporter(
    model: string,
    method: string
  ): IMethodErrorReporter<ParamsError> {
    return MethodError.makeReporter(
      model,
      method,
      (message: string, internal?: Error) =>
        new ParamsError(model, method, message, internal)
    );
  }
}

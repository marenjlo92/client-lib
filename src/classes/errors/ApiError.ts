import { AxiosError } from 'axios';
import { IMethodErrorReporter, MethodError } from './MethodError';

export type AxiosErrorCallback = (e: AxiosError) => Promise<never>;

// errors from server
export class ApiError extends MethodError {
  statusCode: number;

  constructor(
    model: string,
    method: string,
    message: string,
    internal?: Error
  ) {
    super(model, method, message, internal);

    Object.setPrototypeOf(this, ApiError.prototype);
  }

  static is(error: Error): boolean {
    return error instanceof ApiError;
  }

  static reporter(
    model: string,
    method: string
  ): IMethodErrorReporter<ApiError> {
    return MethodError.makeReporter(
      model,
      method,
      (message: string, internal?: Error) =>
        new ApiError(model, method, message, internal)
    );
  }

  static fromAxios(model: string, method: string): AxiosErrorCallback {
    return (e: AxiosError): Promise<never> => {
      const data = (e?.response as any)?.data;
      const [message, statusCode, details] = !data
        ? [e.message, 0, null]
        : [data.message, e.response.status, data.details];

      const ae = new ApiError(model, method, message, e);

      ae.statusCode = statusCode;

      if (details) ae.details = details;

      throw ae;
    };
  }
}

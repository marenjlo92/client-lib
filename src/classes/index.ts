export type { AxiosErrorCallback } from './errors';
export { MethodError, ParamsError, ApiError } from './errors';

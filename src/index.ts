export type {
  ICreateAPIOptions,
  IAPIR,
  IAPIW,
  IAPIRW,
  IAuthorized,
} from './types';
export { MethodError, ParamsError, ApiError } from './classes';
export { default as ContextError } from './contextError';
export {
  cutAndValidate,
  defaultDataDecoration,
  default as CreateAPI,
} from './api';
